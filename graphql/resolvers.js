const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

const resolvers = {
    Query: {
        todoById: async (_, { id }) => {
            return await prisma.todos.findUnique({ where: { id } });
        },
        allTodos: async () => {
            return await prisma.todos.findMany();
        }
    },
    Mutation: {
        updateTodo: async (_, args) => {
            const { id, ...data } = args;
            return await prisma.todos.update({ where: { id }, data });
        },
        deleteTodo: async (_, { id }) => {
            return await prisma.todos.delete({ where: { id } });
        },
        createTodo: async (_, args) => {
            return await prisma.todos.create({ data: args });
        }
    }
};

module.exports = resolvers;
