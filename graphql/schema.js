const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Todo {
    id: ID!
    completed: Boolean
    createdDate: String
    description: String
    duedate: String
    text: String
  }

  type Query {
    todoById(id: ID!): Todo
    allTodos: [Todo]
  }

  type Mutation {
    updateTodo(id: ID!, completed: Boolean, createdDate: String, description: String, duedate: String, text: String): Todo
    deleteTodo(id: ID!): Todo
    createTodo(completed: Boolean, createdDate: String, description: String, duedate: String, text: String): Todo
  }
`;

module.exports = typeDefs;
