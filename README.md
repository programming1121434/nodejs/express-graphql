# ExpressJS Connect to MongoDB via GraphQL

Create .env
```sh 
DATABASE_URL="mongodb+srv=user:password@MongoDB_URL:Port/DB_Name"
```

Install Dependency Package
```sh
npm install
```

Check Connection to MongoDB
```sh
npx prisma db pull
```

Run Express
```sh
node server.js
```

## Remark

- Type Query use for Read data only => GET in REST API

- Type Mutation use for Modify data (Create, Update or Delete) => POST, PUT or DELETE in REST API